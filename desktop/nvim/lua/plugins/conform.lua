return {
  'stevearc/conform.nvim',
  opts = {},
  config = {
      formatters_by_ft = {
          javascript = { "prettierd", "prettier", stop_after_first = true },
      },
  },
}
