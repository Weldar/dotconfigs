vim.o.number = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true
vim.o.smartindent = true

require("config.lazy")
require'lspconfig'.eslint.setup{}
require('lualine').setup()
